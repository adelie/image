====================
 Adélie Image Tools
====================
:Author:
  A\. Wilcox
:Version:
  1.0
:Status:
  Production
:Copyright:
  © 2018-2023 Adélie Linux Team.  NCSA open source licence.



Introduction
============

This repository contains the Adélie Linux image tools, which are used by the
Release Engineering team to create the official Adélie Linux ISO images.

License
```````
The tools contained in this repository are licensed under the NCSA open source
license.

Changes
```````
Any changes to this repository must be reviewed before being pushed to the
current branch.  There are no exceptions to this rule.  For security-sensitive
updates, contact the Security Team.




Usage
=====

This section contains usage information for the software in this repository.


Using ``hscript-image`` to generate an image
````````````````````````````````````````````

The ``configs/`` directory contains the HorizonScript files that describe the
media you can create.  All configurations eventually inherit from the
``base/base.installfile`` HorizonScript.

Each architecture supported for image creation has an entry in ``arch`` used
for all media created for that architecture.

The main targets used are typically:

* ``live/$ARCH-kde.installfile``: Live KDE media
* ``live/$ARCH-lxqt.installfile``: Live LXQt media
* ``live/$ARCH-mate.installfile``: Live MATE media
* ``horizon/$ARCH-horizon.installfile``: Installation media
* ``horizon/$ARCH-horizon-fw.installfile``: Installation media
  (with non-libre firmware)

``hscript-image`` requires the following utilities to be installed:

* apk-tools
* squashfs-tools
* xorriso

Typically, ``hscript-image`` is run in the following way to generate,
for example, KDE media::

    hscript-image -t iso -o adelie-$RELEASE-$ARCH-kde.iso configs/live/$ARCH-kde.installfile


How CDs work
````````````

See the comprehensive documentation on Horizon's ISO target in that repository.
As a cheat-sheet:

* ``-b issue-path=/path/to/issue/file`` - override ``/etc/issue`` on CD
* ``-b iso-params=foo`` - append ``foo`` to ``xorriso`` parameter



Reporting Issues
================

If you have issues installing with the official media, you should report your
issue to `the BTS`_.

If you have issues with generating your own media, or want to discuss other
media types, please contact the development team.

.. _the BTS: http://bts.adelielinux.org/
