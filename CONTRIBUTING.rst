=================================================
 Contribution Guide for Adélie Linux Image Tools
=================================================
:Author:
  * **A. Wilcox**, documentation writer
:Status:
  Final
:Copyright:
  © 2016-2023 Adélie Linux Team.  NCSA open source licence.




Introduction
============

This repository contains the Adélie Linux image tools, which are used by the
Release Engineering team to create the official Adélie Linux ISO images.

License
```````
The tools contained in this repository are licensed under the NCSA open source
license.

Changes
```````
Any changes to this repository must be reviewed before being pushed to the
current branch.  There are no exceptions to this rule.  For security-sensitive
updates, contact the Security Team.




Patch Format
============

This section describes the format required for patches in this repository.


Unifed Diff Format
``````````````````

Patches must be submitted in unifed diff format (``diff -Nu``).  There are no
exceptions to this rule.


Size
````

Patch files must be under 50 Kbyte, and under 500 lines.


Directory Level
```````````````

All patch files must be appliable using ``patch -p1``.  This means that the
``diff`` command must be run one directory above the package's root.


Comments
````````

It is highly encouraged that you use the top matter of your patch file to
describe the changes being made.  This grants other developers and the Adélie
Linux team the same level of understanding that you have over the patch set.
The format provided by ``svn`` or ``git`` is acceptable, and a direct upstream
commit number is even better.




Contributing Changes
====================

This section describes the usual flows of contribution to this repository.


GitLab Pull Requests
````````````````````

#. If you do not already have a GitLab account, you must create one.

#. Create a *fork* of the packages repository.  For more information, consult
   the GitLab online documentation.

#. Clone your forked repository to your computer.

#. Make your changes.

#. Test your changes to ensure they are correct.

#. Add (or remove) changed files using ``git add`` and ``git rm``.

#. Commit your changes to the tree using the command ``git commit`` and
   ``git push``.

#. Visit your forked repository in a Web browser.

#. Choose the *Create Pull Request* button.

#. Review your changes to ensure they are correct, and then submit the form.


Mailing List
````````````

#. Clone the packages repository to your computer.

#. Make your changes.

#. Test your changes to ensure they are correct.

#. Add (or remove) changed files using ``git add`` and ``git rm``.

#. Commit your changes to the tree using the command ``git commit``.

#. Use the command ``git format-patch HEAD^`` to create a patch file for your
   commit.

   .. note:: If you have made multiple commits to the tree, you will need to
             add an additional ^ for each commit you have made.  For example,
             if you have made three commits, you will use the command
             ``git format-patch HEAD^^^``.

#. Email the resulting patch to the release engineering mailing list.
