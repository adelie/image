VERSION ?= 1.0-beta5-$(shell date +%Y%m%d)

help:
	@printf "This Makefile simplifies creation of Adélie Linux media.\n\n"
	@printf "The following targets are recognised:\n\n"
	
	@printf "\tinst\tGenerate 'install' CD images.\n"
	@printf "\tkde\tGenerate a live KDE CD image.\n"
	@printf "\tlxqt\tGenerate a live LXQt CD image.\n"
	@printf "\tmate\tGenerate a live MATE CD image.\n"
	@printf "\txfce\tGenerate a live XFCE CD image.\n"
	@printf "\tiso\tGenerate all possible CD images.\n"
	@printf "\tfull\tGenerate a full rootfs tar.xz.\n"
	@printf "\tmini\tGenerate a mini rootfs tar.xz.\n"
	@printf "\ttar\tGenerate both rootfs tar.xz files.\n"
	@printf "\tworld\tGenerate all above media types.\n"
	
	@printf "\nYou must set ARCH to a supported architecture.\n"
	@printf "Note that if you don't set ARCH to the native arch of your host,\nyou probably need qemu-user installed.\n"
	@printf '\nVERSION will default to: %s\n' $(VERSION)

adelie-inst-firmware-$(ARCH)-$(VERSION).iso: configs/horizon/$(ARCH)-horizon-fw.installfile configs/horizon/firmware.installfile configs/horizon/horizon.installfile configs/arch/$(ARCH).installfile configs/base/base.installfile
	hscript-image -t iso -o adelie-inst-firmware-$(ARCH)-$(VERSION).iso configs/horizon/$(ARCH)-horizon-fw.installfile

adelie-inst-$(ARCH)-$(VERSION).iso: configs/horizon/$(ARCH)-horizon.installfile configs/horizon/horizon.installfile configs/arch/$(ARCH).installfile configs/base/base.installfile
	hscript-image -t iso -o adelie-inst-$(ARCH)-$(VERSION).iso configs/horizon/$(ARCH)-horizon.installfile

inst: adelie-inst-firmware-$(ARCH)-$(VERSION).iso adelie-inst-$(ARCH)-$(VERSION).iso

adelie-live-%-$(ARCH)-$(VERSION).iso: configs/live/$(ARCH)-%.installfile configs/base/%.installfile configs/arch/$(ARCH).installfile configs/base/base.installfile
	hscript-image -t iso -o adelie-live-$*-$(ARCH)-$(VERSION).iso configs/live/$(ARCH)-$*.installfile

kde: adelie-live-kde-$(ARCH)-$(VERSION).iso
lxqt: adelie-live-lxqt-$(ARCH)-$(VERSION).iso
mate: adelie-live-mate-$(ARCH)-$(VERSION).iso
xfce: adelie-live-xfce-$(ARCH)-$(VERSION).iso

iso: inst kde lxqt mate xfce

adelie-rootfs-$(ARCH)-$(VERSION).txz: configs/tarballs/$(ARCH)-full.installfile configs/base/base.installfile
	hscript-image -t txz -o adelie-rootfs-$(ARCH)-$(VERSION).txz configs/tarballs/$(ARCH)-full.installfile

adelie-minirootfs-$(ARCH)-$(VERSION).txz: configs/tarballs/$(ARCH)-mini.installfile configs/tarballs/mini.installfile
	hscript-image -t txz -o adelie-minirootfs-$(ARCH)-$(VERSION).txz configs/tarballs/$(ARCH)-mini.installfile

full: adelie-rootfs-$(ARCH)-$(VERSION).txz
mini: adelie-minirootfs-$(ARCH)-$(VERSION).txz

tar: full mini

world: iso tar

all: world

.PHONY: iso inst kde lxqt mate xfce tar full mini world

